```mermaid
graph TD;

    BR((Browser));
    FE((Frontend));
    BE((Backend));
    BR -->|Sends requests| FE;
    FE -->|Components| BE;
    FE -->|Views| BE;
    FE -->|Routing| BE;
    FE -.->|Renders webapp| BR;
    BE -->|Runs migrations| DB(Database);
    BE -->|Starts websocket server| WS(Websocket Server);
    WS -->|Websocket chat| CH;
    WS -->|Notification Service| NS;
    BE -.->|Sends data| FE;
    CH -->|Access userid and releveant info|DB;
    NS -->|Access userid and relevant info for group, chat, follow status|DB;
    DB -.->|Serve data based on auth and user id| BE;
    NS -.-> BE;
    CH -.-> BE;
    DB((Database));
    WS((Websocket Server));
    NS((Notification Service));
    CH((Chat service));
    style FE fill:#fff,stroke:#333,stroke-width:2px;
    style BE fill:#fff,stroke:#333,stroke-width:2px;
    style DB fill:#fff,stroke:#333,stroke-width:2px;
    style WS fill:#fff,stroke:#333,stroke-width:2px;
    style NS fill:#fff,stroke:#333,stroke-width:2px;
    style CH fill:#fff,stroke:#333,stroke-width:2px;

```
