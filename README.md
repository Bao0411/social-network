## social-network


## Run the project
To test the project you need to have [NodeJS](https://nodejs.org/en/) installed.
1. Start frontend server by going to **/frontend** directory and running those commands
-  `npm install`
-  `npm run serve`

2. Start backend server by going to **/backend** directory and run `go run server.go`

3. Or run the docker script

- `./docker_script.sh`



## Stack


Frontend
- Vue
- HTML & CSS


Backend
- Go
- SQLite3


