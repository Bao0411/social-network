package db

import (
	"database/sql"
	"log"
	"os"
	"social-network/pkg/models"

	"fmt"

	_ "github.com/mattn/go-sqlite3"
	migrate "github.com/rubenv/sql-migrate"
)

// initialize db
func InitDB() *sql.DB {

	db, err := sql.Open("sqlite3", "./tempDB.db")
	if err != nil {
		log.Fatal(err)
	}

	err = Migrations(db)
	if err != nil {
		log.Fatal(err)
	}

	return db
}

// InitRepositories should be called in server.go
// creates connection to db for all rep
func InitRepositories(db *sql.DB) *models.Repositories {
	return &models.Repositories{
		UserRepo:    &UserRepository{DB: db},
		SessionRepo: &SessionRepository{DB: db},
		GroupRepo:   &GroupRepository{DB: db},
		PostRepo:    &PostRepository{DB: db},
		CommentRepo: &CommentRepository{DB: db},
		NotifRepo:   &NotifRepository{DB: db},
		EventRepo:   &EventRepository{DB: db},
		MsgRepo:     &MsgRepository{DB: db},
	}
}

func Migrations(db *sql.DB) error {

	currentPath, err := os.Getwd()
	if err != nil {
		fmt.Printf("Error getting current working directory: %s\n", err)
		return err
	}

	fmt.Printf("Current working directory: %s\n", currentPath)

	migrationsDir := "pkg/db/migration/sqlite"
	fmt.Printf("Using migrations directory: %s\n", migrationsDir)

	if _, err := os.Stat(migrationsDir); os.IsNotExist(err) {
		fmt.Printf("Migrations directory does not exist: %s\n", migrationsDir)
		return err
	}

	migrations := &migrate.FileMigrationSource{
		Dir: migrationsDir,
	}

	fmt.Println("Executing migrations...")
	n, err := migrate.Exec(db, "sqlite3", migrations, migrate.Up)
	if err != nil {
		fmt.Printf("Error executing migrations: %s\n", err)
		return err
	}

	fmt.Printf("Applied %d migrations to database.db!\n", n)

	return nil
}
